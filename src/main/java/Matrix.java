public class Matrix {

    private int[][] matrix;
    private int size;

    public Matrix(int size){
        matrix = new int[size][size];
        this.size=size;
        for (int i = 0; i < size; i++)
            matrix[i][i] =1;
    }

    public int getSize(){ return size; }

    public void setElement(int row, int col, int value){
        if(row < 0 || row >= matrix.length || col < 0 || col >= matrix.length)
            throw new ArrayIndexOutOfBoundsException("Incorrect indexes");
        matrix[row][col]=value;
    }

    public int getElement(int row, int col){
        if(row < 0 || row >= matrix.length || col < 0 || col >= matrix.length)
            throw new ArrayIndexOutOfBoundsException("Incorrect indexes");
        return matrix[row][col];
    }

    public Matrix sum(Matrix cur) {
        if(this.size != cur.size)
            throw new RuntimeException("Different sizes");
        Matrix res = new Matrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++)
                res.matrix[i][j] = cur.matrix[i][j]+this.matrix[i][j];
        }
        return res;
    }

    public Matrix product(Matrix cur)  {
        if(this.size != cur.size)
            throw new RuntimeException("Different sizes");
        Matrix res = new Matrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                res.matrix[i][j]=0;
                for (int k = 0; k < size; k++) {
                    res.matrix[i][j]+=this.matrix[i][k]*cur.matrix[k][j];
                }
            }
        }
        return res;
    }
    public Matrix pow(int n)  {
        Matrix res = new Matrix(size);
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                res.matrix[i][j] = matrix[i][j];
            }
        }
        for (int i = 0; i < n-1; i++) {
            res = res.product(this);
        }
        return res;
    }

    public String toString(){
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < size; i++) {
            for (int j = 0; j < size; j++) {
                sb.append(matrix[i][j]).append(" ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }
}
