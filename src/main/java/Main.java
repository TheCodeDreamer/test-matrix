public class Main {
    public static void main(String[] args)  {
        Matrix mtrx = new Matrix(2);
        mtrx.setElement(0,1,1);
        mtrx.setElement(1,0,1);
        mtrx.setElement(1,1,0);
        mtrx.setElement(1,1,0);
        for (int i = 1; i <= 10; i++) {
            System.out.println("#" + i + " power");
            System.out.println(mtrx.pow(i));
        }
    }
}

